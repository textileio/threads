# Threads iOS App
![](https://gitlab.com/weareset/threads/raw/master/Icon.png)

## About
Threads is a simple app created by [Textile](https://textile.io). It serves as an example of how to integrate Textile's Loom SDK into an app. You can run Threads on your own devices as a quick and painless way to start generating your own data to explore in the Textile data science platform.

## Requirements
* Threads requires that you have [Cocoapods](https://cocoapods.org) installed... It's used to install Threads' dependencies.
* Threads was built with a current version of Xcode. Things should go smoothly if you use a similarly new version.

## How to Build and Run Threads
There are just a couple things you need to do in order to build and run Threads:

1. Clone this repository.
2. Using the Terminal, switch directories into the cloned project and run `pod repo update` and `pod install`. This installs Textile's Loom SDK and other project dependencies.
3. Open `Threads.xcworkspace` in Xcode.
4. Select the Threads target and supply a valid Bundle Identifier and Team.
5. Head over to the [Textile web application](https://mill.textile.io), create an account if needed, log in, and create a new Textile client.
6. Be sure to enter your APN credentials in the settings of the Textile client you just created. More detailed information available in the [Textile documentation](http://docs.textile.io).
7. Back in Xcode, open `AppDelegate.swift`, copy and paste the client id and secret for the new Textile client you just created into the `Credentials(clientId:clientSecret:)` constructor.
8. Run it.

## Additional Information
You can learn more about Textile and the Loom SDK at the [Textile website](https://textile.io) and in the [Textile documentation](http://docs.textile.io).

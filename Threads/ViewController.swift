import Foundation
import UIKit
import EasyPeasy

class ViewController: UIViewController {

  var showLabel = false

  private lazy var label: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = .white
    label.font = UIFont(name: "HelveticaNeue", size: 17)
    label.text = "Success!"
    return label
  }()

  private lazy var imageView: UIImageView = {
    let imageView = UIImageView(image: UIImage(named: "Logo"))
    return imageView
  }()

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .clear

    view.addSubview(imageView)
    imageView.easy.layout(
      Center()
    )

    view.addSubview(label)
    label.easy.layout(
      CenterX(),
      Bottom(44).to(imageView, .top)
    )
    label.isHidden = !showLabel

    NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    UIView.animate(withDuration: 2, delay: 8, options: [.curveEaseOut], animations: {
      self.label.alpha = 0
    }, completion: nil)

    animateImageView()
  }

  private func animateImageView() {
    UIView.animateKeyframes(withDuration: 3, delay: 3, options: [.calculationModeCubic, .repeat], animations: {
      UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.005, animations: {
        self.imageView.transform = CGAffineTransform(scaleX: 1.03, y: 1.03)
      })
      UIView.addKeyframe(withRelativeStartTime: 0.005, relativeDuration: 0.015, animations: {
        self.imageView.transform = .identity
      })
      UIView.addKeyframe(withRelativeStartTime: 0.020, relativeDuration: 0.004, animations: {
        self.imageView.transform = CGAffineTransform(scaleX: 1.03, y: 1.03)
      })
      UIView.addKeyframe(withRelativeStartTime: 0.024, relativeDuration: 0.01, animations: {
        self.imageView.transform = .identity
      })
    }, completion: nil)
  }

  @objc private func didEnterBackground() {
    imageView.layer.removeAllAnimations()
  }

  @objc private func willEnterForeground() {
    animateImageView()
  }
}

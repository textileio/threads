import UIKit
import Loom

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = PageController()
    window?.makeKeyAndVisible()

    let credentials = Credentials(
      clientId: "<Your Textile Cient ID>",
      clientSecret: "<Your Textile Cient Secret>"
    )
    Loom.authorize(with: credentials) { error in
      if let error = error {
        print("Got an error authorizing Loom: \(error.localizedDescription)")
      }
    }

    return true
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    Loom.registeredForPushNotifications(withToken: deviceToken)
  }

  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    Loom.failedToRegisterForPushNotifications(withError: error)
  }

  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    Loom.receivedRemoteNotification(userInfo: userInfo, fetchCompletionHandler: completionHandler)
  }
}


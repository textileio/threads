import UIKit
import EasyPeasy

class ActionViewController: UIViewController {

  var text: String?
  var buttonTitle: String?
  var onComplete: (() -> Void)?

  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 1
    label.textColor = .white
    label.textAlignment = .center
    label.font = button.titleLabel?.font
    label.text = title
    return label
  }()

  private lazy var label: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = .white
    label.font = UIFont(name: "HelveticaNeue", size: 17)
    label.text = text
    return label
  }()

  private lazy var button: UIButton = {
    let button = UIButton(type: .system)
    button.tintColor = .white
    button.setTitle(buttonTitle?.uppercased(), for: .normal)
    button.isEnabled = false
    return button
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .clear

    view.addSubview(titleLabel)
    titleLabel.easy.layout(
      Height(44),
      Top(16).to(view, .topMargin),
      Left(0).to(view, .leftMargin),
      Right(0).to(view, .rightMargin)
    )

    view.addSubview(label)
    label.easy.layout(
      Top(16).to(titleLabel, .topMargin),
      Left(0).to(view, .leftMargin),
      Right(0).to(view, .rightMargin)
    )

    view.addSubview(button)
    button.easy.layout(
      Height(44),
      Left(0).to(view, .leftMargin),
      Right(0).to(view, .rightMargin),
      Bottom(44).to(view, .bottomMargin),
      Top(16).to(label, .bottom)
    )
    button.addTarget(self, action: #selector(complete), for: .touchUpInside)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      self.button.isEnabled = true
    }
  }

  @objc private func complete() {
    onComplete?()
  }
}

